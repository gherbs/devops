#!/bin/bash
if [ -f /Users/georgeshields/.bash_profile ]; then
    . /Users/georgeshields/.bash_profile
fi
####> batch build must run as user for key chain!
set -x
export BUILDLOC=/Users/georgeshields/pasco-software/devops/CICD/
export Buildtype=-PRelease
export Buildtype=

function updateGit(){
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree fetch origin 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree pull --rebase origin develop 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree submodule update --init 	
}

echo "BUILD ##starting #2 Android on x"
echo "May need to Start android studio to build"
echo " build apk"
cd /Users/georgeshields/pasco-software/SPARKvue/SPARKvue/platforms/android
updateGit
gradle wrapper
cd /Users/georgeshields/pasco-software/SPARKvue

rm $BUILDLOC/logs/build_android.log
gradle buildAndroid $Buildtype > $BUILDLOC/logs/build_android.txt
cp /Users/georgeshields/pasco-software/SPARKvue/spark-installers/Android/SPARKvue.zip $BUILDLOC/binout
