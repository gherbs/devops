#!/bin/bash
if [ -f /Users/georgeshields/.bash_profile ]; then
    . /Users/georgeshields/.bash_profile
fi
####> batch build must run as user for key chain!
export BUILDLOC=/Users/georgeshields/pasco-software/devops/CICD/
#export Buildtype=-PRelease
export Buildtype=

function updateGit(){
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree fetch origin 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree pull --rebase origin develop 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree submodule update --init 	
}

echo "BUILD ##starting #1 ios on Mac"
echo "Plug in you ipad or it will fail!"
cd /Users/georgeshields/pasco-software/SPARKvue
updateGit
# missing cusotm xcodebuild for no device
#gradle buildiOS -PRelease 
rm $BUILDLOC/logs/build_spark_ios.txt

gradle buildiOS $Buildtype > $BUILDLOC/logs/build_spark_ios.txt
# cp ???? ##ios need to go to test-flight via xcode??