#!/bin/bash
if [ -f /Users/georgeshields/.bash_profile ]; then
    . /Users/georgeshields/.bash_profile
fi
####> batch build must run as user for key chain!
export BUILDLOC=/Users/georgeshields/pasco-software/devops/CICD/
export Buildtype=-PRelease
#export Buildtype=

function updateGit(){
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree fetch origin 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree pull --rebase origin develop 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree submodule update --init 	
}

echo "BUILD ##starting #3 SPARKvue on Mac"
#we use npm install jade@1.11.0
cd /Users/georgeshields/pasco-software/SPARKvue
updateGit
npm install
rm $BUILDLOC/logs/build_spark_mac.txt

gradle buildMac $Buildtype > $BUILDLOC/logs/build_spark_mac.txt
cp /Users/georgeshields/pasco-software/SPARKvue/spark-installers/Mac/Packages/build/SPARKvue.dmg $BUILDLOC/binout
