#!/bin/bash
if [ -f /Users/georgeshields/.bash_profile ]; then
    . /Users/georgeshields/.bash_profile
fi
####> batch build must run as user for key chain!
# run from /DEVOPS/schedule
set -x
export BUILDLOC=/Users/georgeshields/pasco-software/devops/CICD/

cd $BUILDLOC/logs
rm daily_summary.txt
echo "*** Report of warnings builds:" > daily_summary.txt
sift "daily" *.txt >> daily_summary.txt
echo "*** end of warnings ***" >> daily_summary.txt
echo " "
echo "*** Report of successful builds and build :" >> daily_summary.txt
sift "BUILD SUCCESSFUL" *.txt  >> daily_summary.txt
sift "BUILD SUCCEEDED" *.txt  >> daily_summary.txt
echo "*** end of successful ***" >> daily_summary.txt 

exit

echo " "
echo "*** Report of failed builds" >> daily_summary.txt
#tail 
#sift " FAILED" *.log  >> daily.txt
echo "*** end of failed ***" >> daily_summary.txt 

echo "email report " 
#$BUILDLOC/bin/emailDaily
# save off on NAS
#cp  $BUILDLOC/production/* ~/software/Devops/builds
#cp  $BUILDLOC/logs/* ~/software/builds/logs
