#!/bin/bash
if [ -f /Users/georgeshields/.bash_profile ]; then
    . /Users/georgeshields/.bash_profile
fi
####> batch build must run as user for key chain!
set -x
function pause(){
   read -p "$*"
}

function updateGit(){
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree fetch origin 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree pull --rebase origin develop 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree submodule update --init 	
}



#debug
exit
# call windows vm and drive not with sikulix or chromedp, but with circuit!
# rdesktop -u SirIsaac -p SirIsaac 172.16.2.2
#https://www.howtogeek.com/213145/how-to%C2%A0convert-a-physical-windows-or-linux-pc-to-a-virtual-machine/
#VMware vCenter Converter fails used diskvhd
echo " remote builds for windows TBD"
##Requires Windows and Install Shield
echo "BUILD ##starting #6 Sparkvue on windows"
cd pasco-software/SPARKvue
echo " build with out installer due to installshield licensing "
#gradle buildWindows -P
gradle buildWindows

echo "BUILD ##starting #7 Capstone on windows"
cd pasco-software/Capstone
#gradle buildWindows -P
gradle buildWindows
