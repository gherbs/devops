#!/bin/bash
if [ -f /Users/georgeshields/.bash_profile ]; then
    . /Users/georgeshields/.bash_profile
fi
####> batch build must run as user for key chain!
export BUILDLOC=/Users/georgeshields/pasco-software/devops/CICD/
export Buildtype=-PRelease
#export Buildtype=

function updateGit(){
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree fetch origin 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree pull --rebase origin develop 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree submodule update --init 	
}


echo "BUILD ##starting #4 chrome SPARKvue, these could be on linux"
cd /Users/georgeshields/pasco-software/SPARKvue
updateGit
rm $BUILDLOC/logs/build_spark_chrome.txt

gradle buildChrome $Buildtype > $BUILDLOC/logs/build_spark_chrome.txt

cp /Users/georgeshields/pasco-software/SPARKvue/spark-installers/Chrome/SPARKvue-Chrome.zip $BUILDLOC/binout