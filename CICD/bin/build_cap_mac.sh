#!/bin/bash
if [ -f /Users/georgeshields/.bash_profile ]; then
    . /Users/georgeshields/.bash_profile
fi
####> batch build must run as user for key chain!
#set -x
export BUILDLOC=/Users/georgeshields/pasco-software/devops/CICD/
export Buildtype=-PRelease
#export Buildtype=

function updateGit(){
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree fetch origin 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree pull --rebase origin develop 
	git -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree submodule update --init 	
}

echo "BUILD ##starting #5 Capstone on Mac"
echo "fix mac QT 32 for mac, still not 64"
echo " builds for capstone require edit of XcodeQtConfig.cap32"
cd /Users/georgeshields/pasco-software/CAPstoneApp
updateGit
## may need to swap config for old QT
#cp /Users/georgeshields/pasco-software/CAPstoneApp/PascoLibs/XCodeQtConfig.xcconfig /Users/georgeshields/Documents/DEVOPS
#cp /Users/georgeshields/Documents/DEVOPS/XcodeQtConfig.cap32 /Users/georgeshields/pasco-software/CAPstoneApp/PascoLibs/XCodeQtConfig.xcconfig
rm $BUILDLOC/logs/build_cap_mac.txt
gradle buildMac $Buildtype > $BUILDLOC/logs/build_cap_mac.txt

#echo " restore XcodeQtConfig.xcconfig"
#cp /Users/georgeshields/Documents/DEVOPS/XcodeQtConfig.xcconfig /Users/georgeshields/pasco-software/CAPstoneApp/PascoLibs/XCodeQtConfig.xcconfig

cp /Users/georgeshields/pasco-software/CAPstoneApp/Installers/Capstone/Macintosh/build/*.dmg $BUILDLOC/binout