#!/bin/bash
if [ -f /Users/georgeshields/.bash_profile ]; then
    . /Users/georgeshields/.bash_profile
fi
export BUILDLOC=/Users/georgeshields/pasco-software/devops/CICD/
# mount keep disconnecting
#mount_smbfs //gshields@p23/software /Volumes/software
# no android project??
#$BUILDLOC/bin/build_spark_android.sh

# no ipad to plug in 
$BUILDLOC/bin/build_spark_ios.sh

$BUILDLOC/bin/build_spark_chrome.sh

$BUILDLOC/bin/build_spark_mac.sh

# no key to sign
$BUILDLOC/bin/build_cap_mac.sh

# report results
$BUILDLOC/bin/build_report.sh
