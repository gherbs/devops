package main

import (
	"fmt"
	"os"

	"gopkg.in/gomail.v2"
)

// ExitErr template for errors
func ExitErr(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func main() {
	m := gomail.NewMessage()
	m.SetHeader("From", "tsupportjsd@pasco.com")
	m.SetHeader("To", "gshields@pasco.com")
	//m.SetHeader("To", "DevOpBuild@pasco.com")

	//m.SetAddressHeader("Cc", "gshields@pasco.com")
	m.SetHeader("Subject", "Nightly Developer build reports for source head")
	//m.SetBody("text/plain", "Binarys are in //P23/Software/Devops/builds, Full build reports are in ./logs ")
	m.SetBody("text/plain", "Enclosed are the Summary report and detail build logs, PLEASE NOTE THESE ARE DEVELOPER BUILDS, NOT A RELEASE BUILDS")

	m.Attach("/Users/georgeshields/pasco-software/devops/CICD/logs/daily_summary.txt")
	m.Attach("/Users/georgeshields/pasco-software/devops/CICD/logs/build_spark_ios.txt")
	m.Attach("/Users/georgeshields/pasco-software/devops/CICD/logs/build_android.txt")
	m.Attach("/Users/georgeshields/pasco-software/devops/CICD/logs/build_spark_chrome.txt")
	m.Attach("/Users/georgeshields/pasco-software/devops/CICD/logs/build_spark_mac.txt")
	m.Attach("/Users/georgeshields/pasco-software/devops/CICD/logs/build_cap_mac.txt")

	d := gomail.NewPlainDialer("pas-xcng.Roseville.pasco.com", 25, "tsupportjsd@pasco.com", "Bigbang!1")

	// Send the email
	err := d.DialAndSend(m)
	ExitErr(err)
}
