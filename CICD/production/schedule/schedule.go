package main

import (
	//"fmt"
	"fmt"
	"os"
	"os/exec"

	"../../../localization/production/common"
)

func runIT(a string, b []string) {
	fmt.Println(b[1])
	if err := exec.Command(a, b...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		//common.Debug("Command Failed")
		//os.Exit(1)
	}
	common.Debug("done")
}

func main() {
	//replaces bash as gradle does not wait and build system has to run in serial execution due to overlapping workspaces
	cmd := "bash"
	// this program TRIED to run on the local machine via crontab
	/* crontab FAILS!! on mac
	/Users/georgeshields/pasco-software/devops/CICD/bin/emailDaily
	/Users/georgeshields/bin/save-go-list.sh
	*/
	// NAS HAS PROBLEM SO SAVE ON LOCAL MACHINE
	// mkdir Software
	// mount_smbfs //gshields:sdkP6627@172.16.1.223/Software Software/
	// umount Software

	// use /etc/auto_mount for flaky Qnap device raid NAS, which disconnects after timeout for OSX
	// need to have root access to copy?
	// BUILDLOC=/Users/georgeshields/pasco-software/devops/CICD/
	// BUILDLOC=/Users/georgeshields/Documents/work_src/CICD/
	args := []string{"-c", "/Users/georgeshields/pasco-software/devops/CICD/bin/build_spark_android.sh"}
	runIT(cmd, args)

	args = []string{"-c", "/Users/georgeshields/pasco-software/devops/CICD/bin/build_spark_ios.sh"}
	runIT(cmd, args)

	args = []string{"-c", "/Users/georgeshields/pasco-software/devops/CICD/bin/build_spark_chrome.sh"}
	runIT(cmd, args)

	args = []string{"-c", "/Users/georgeshields/pasco-software/devops/CICD/bin/build_spark_mac.sh"}
	runIT(cmd, args)

	args = []string{"-c", "/Users/georgeshields/pasco-software/devops/CICD/bin/build_cap_mac.sh"}
	runIT(cmd, args)

	args = []string{"-c", "/Users/georgeshields/pasco-software/devops/CICD/bin/build_report.sh"}
	runIT(cmd, args)

}
