# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Common library to handle the international Languages
* 1.0
*

### How do I get set up? ###

* Install GO 1.9, (edit with visual studio code or liteide,  both of which are free)
* Install GO packages of ("github.com/go-sql-driver/mysql","github.com/jmoiron/sqlx","gopkg.in/gomail.v2","github.com/tealeg/xlsx"),
* Mysql 5.7 (free) admin editor for Mysql (either Sequel Pro or PhpMyAdmin, both of which are free).
* Pull source from GIT branch
* Database is in /stringsDB/data, make a backup before any operation
* All input for change will come from Google sheet, until a web app ca be developed
* See this confluance page to understand design and implementatio
* https://confluence.pasco.com/display/ENG/Localization+Process+of+Languages#?lucidIFH-viewer-7241bee5=1


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
