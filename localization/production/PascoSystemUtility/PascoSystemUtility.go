package main

import (
	"database/sql"

	"fmt"
	"os"

	"../common"
	"github.com/jmoiron/sqlx"
)

var testrun = false

/// main PascoSystemUtility.go, a program for creating xml output needed for android development
func main() {
	//var translateTmp = false

	db, ferr := sqlx.Open("mysql", "root:3tring3DB@@@tcp(127.0.0.1:3306)/sourceLocalize?charset=utf8")
	common.ExitErr(ferr)
	defer db.Close()

	// for each language
	stmt := "SELECT pascoID, ISO3166 FROM code"
	langs, lerr := db.Query(stmt)
	common.ExitErr(lerr)
	defer langs.Close()

	var filename string
	var pascoID, ISO3166 sql.NullString

	for langs.Next() {
		lerr = langs.Scan(&pascoID, &ISO3166)
		common.ExitErr(lerr)

		//fmt.Printf("processing lang : %v\n", pascoID.String)
		filename = fmt.Sprintf("../../dataout/PascoSystemUtility/values-%v/strings.xml", ISO3166.String)
		fhandle, ferr := os.Create(filename)
		common.ExitErr(ferr)
		defer fhandle.Close()

		stmt = fmt.Sprintf("SELECT COUNT(*) as count FROM strings where (sectionID=105) AND pascoID='%v' AND state!=99 ", pascoID.String)
		results, err := db.Query(stmt)
		common.ExitErr(err)
		defer results.Close()

		// index to see when we have 1st line for json
		var countRows int
		for results.Next() {
			err := results.Scan(&countRows)
			common.ExitErr(err)
		}
		err = results.Err()
		common.ExitErr(err)
		results.Close()
		//end of checkCount

		stmt = fmt.Sprintf("SELECT stringID, stringValue, pascoID FROM strings WHERE (sectionID=105) AND pascoID='%v'AND state!=99 ", pascoID.String)

		rows, err := db.Query(stmt)
		common.ExitErr(err)
		defer rows.Close()

		var count = 1
		var stringID, stringValue, pascoID sql.NullString

		for rows.Next() {
			err = rows.Scan(&stringID, &stringValue, &pascoID)
			common.ExitErr(err)

			// core output
			if count == 1 {
				// header
				fmt.Fprintf(fhandle, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<resources>\n")
			}

			key := common.StrID2strKey(db, stringID.String) // need to lookup stringKey for stringID

			if count <= countRows {
				fmt.Fprintf(fhandle, "\t<string name=\"%s\">%s</string>\n", key, stringValue.String)
				fmt.Fprintf(fhandle, "\t<!--\"%s\"-->\n", stringID.String)
			}
			// end core
			count++
		}
		err = rows.Err()
		common.ExitErr(err)

		// extra fiddly bit to close json
		fmt.Fprintf(fhandle, "</resources>")
	}
	err := langs.Err()
	common.ExitErr(err)

}
