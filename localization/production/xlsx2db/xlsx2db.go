package main

import (
	"fmt"
	//"database/sql"
	//"os"
	"strconv"
	"time"

	"../common"
	_ "github.com/go-sql-driver/mysql" //needed for database
	"github.com/jmoiron/sqlx"
	"github.com/tealeg/xlsx"
)

// make these global! prevent unused variable error message
var stringID, stringKey, stringValue, pascoID, state, creation, modification, certified, sectionID string //for string table
var appsID, arguments, description string                                                                 //for source table
var appName, path, localizerNotes, devNotes, category, subcategory string                                 //for appInstances table
var stringValueX, key, dbValue string
var strID int

/// main() xlsx2db.go, a program that imports an excel file into the mysql database
/// uses the runtime.ini file to control mode and file name imported
/// there are two types of files that are imported, the Gsep and the develop xls format
/// the developer format has two modes one for creating(adding) new data
/// the other developer mode is for updating exsisting strings that have entries in source and addInstance tables
func main() {
	var devupxls, devaddxls, gsepxls bool

	name, param1, _, _, _ := common.Getini()

	fmt.Printf("processing %s\n", name)
	switch param1 {
	case "devup": // 1 exsisting language column
		devupxls = true
	case "devadd": // 1 new language column
		devaddxls = true
		devupxls = true
	case "gsep": // All exsisting languages
		gsepxls = true
	}
	fmt.Printf("operation %s\n", param1)
	//os.Exit(1)

	t := time.Now()
	currentTime := t.Format("20060102150405") // template for time, to get yyyyMMddHHmmss, never change this!
	db, ferr := sqlx.Open("mysql", "root:3tring3DB@@@tcp(127.0.0.1:3306)/sourceLocalize?charset=utf8")
	common.ExitErr(ferr)
	defer db.Close()

	// IMPORT CHECK FLAG SETTING BELOW BEFORE YOU RUN, FOR IMPORT FORMAT
	// use testrun in common.ExecSQL to debug
	var historyID = 41 // build release number, update for season release
	offset := 0
	xlFile, err := xlsx.OpenFile(name) // date of file
	common.ExitErr(err)

	// xlsx must be checked before use, common.Valueclean catches some but utf8 is tricky
	// mysqldump -u root --password=3tring3DB@@ sourceLocalize source strings section code history appInstances category subCategory > productionDB.sql
	// mysql -u root --password=3tring3DB@@ sourceLocalize < productionDB.sql

	// sectionID= stringID ranges: 0-Interface =0-172, 1-Sensor =1001-1216, 2-Measurement =2001-2358
	// 3-Unit =3001-3155, 4-Measurement =4359-4548, 5-Symbols =5000-5190,6-Generic =6000-6093,7-Description =7000-7008
	// sparkvue starts at 10000, Capstone starts at 100000 +old excel number, still need to consolidate duplicates (sectionID 99)
	// 101-capstone =105000-120005; 102-sparkvue =10000-36218

	tmpnum := 0
	// the below were trials at manipulating the db, best to use xslx as medium as it allows review
	var dbCopyStrings = false // DB copy strings mode,
	var strValLocal = false   // DB patch mode
	var dbsrcApp = false      // DB patch mode

	// check sheets
	for _, sheet := range xlFile.Sheets {
		for rownum, row := range sheet.Rows { // check rows
			if rownum == 0 { // check row number
				for _, colCell := range row.Cells {
					fmt.Print(colCell, "\t") // visual verify the header of the table
				}

				fmt.Println("\n...above is xslx header format...") // skip header
			} else {

				//TODO VERIFY stringKey DO NOT HAVE SPACES IN THEM
				//TODO VERIFY stringValue for unknown utf8 char that cause QT to barf
				//language in multi col
				if gsepxls == true { //for 3 tables(strings,source,appInstance)
					for colnum, colCell := range row.Cells {
						switch colnum {
						case 0: // xls column a
							sectionID = colCell.String()
						case 1: // xls column b
							stringID = colCell.String()
						case 2: // xls column c
							stringKey = colCell.String()
						case 3: // xls column d
							path = colCell.String()
						case 4: // xls column e
							localizerNotes = colCell.String()
						case 5: // xls column f
							arguments = colCell.String()
						case 6: // xls column g
							devNotes = colCell.String()
						case 7: // xls column h, english
							stringValue = colCell.String()
							// have enough to update english
							if stringID != "" && sectionID != "--" {
								common.Dbsource(db, stringID, stringKey, arguments, description)
								common.DbappIn(db, stringID, historyID, path, localizerNotes, devNotes)                                   //  historyID orginator of string
								common.Dbstrings(db, stringID, stringValue, "enu", "4", currentTime, currentTime, currentTime, sectionID) // plug pascoID=enu,state=4
							}
						//  xls columns for other langages
						case 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35: // Gsep lang col
							//lookup pascode from col-3, add index to code
							stringValueX = colCell.String()
							colInt := strconv.Itoa(colnum)
							pascoID = common.ColNum2PascoID(db, colInt)
							state = "4"
							//fmt.Printf("Xval:%v id:%v secID:%v \n", stringValueX, strID, sectionID)
							if sectionID != "--" && stringID != "" && pascoID != "" {
								common.Dbstrings(db, stringID, stringValueX, pascoID, state, currentTime, currentTime, currentTime, sectionID) // plug pascoID,state=4
							} else {
								fmt.Printf("val:%v id:%v secID:%v \n", stringValueX, strID, sectionID)
							}
						}
					}
				}

				// Developer import as languages in single column,
				if devupxls == true { //for 3 tables(strings,source,appInstance)
					for colnum, colCell := range row.Cells {
						switch colnum {
						case 0: // xls column a
							sectionID = colCell.String()
						case 1: // xls column b
							stringID = colCell.String()
							tmpnum, _ = strconv.Atoi(stringID)
							tmpnum = tmpnum + offset
						case 2: // xls column c
							stringKey = colCell.String()
						case 3: // xls column d
							pascoID = colCell.String()
						case 4: // xls column e
							state = colCell.String()
						case 5: // xls column f
							devNotes = colCell.String()
						case 6: // xls column g
							description = colCell.String()
						case 7: // xls column h
							stringValue = colCell.String()
							// have enough to update
							stringID = strconv.Itoa(tmpnum) // capstone offset
							if sectionID != "--" && stringID != "" && stringKey != "" && pascoID != "*" {
								if devaddxls == true { // a new stringKey is added to application
									common.Dbsource(db, stringID, stringKey, arguments, description)
									common.DbappIn(db, stringID, historyID, path, localizerNotes, devNotes)
								}
								common.Dbstrings(db, stringID, stringValue, pascoID, state, currentTime, currentTime, currentTime, sectionID)
							}
						}
					}
				}

				// DB table copy for strings
				if dbCopyStrings == true { //for 3 tables(strings,source,appInstance)
					for colnum, colCell := range row.Cells {
						switch colnum {
						case 0:
							stringID = colCell.String()
						case 1:
							stringValue = colCell.String()
						case 2:
							pascoID = colCell.String()
						case 3:
							state = colCell.String()
						case 4:
							sectionID = colCell.String()
							// have enough to update
							if stringID != "" && stringValue != "" && pascoID != "" && state != "" && sectionID != "" {
								common.Dbstrings(db, stringID, stringValue, pascoID, state, currentTime, currentTime, currentTime, sectionID)
							}
						}
					}
				}

				// DB table language in single column,
				if strValLocal == true {
					for colnum, colCell := range row.Cells {
						switch colnum {
						case 0:
							stringValueX = colCell.String()
						case 1:
							stringValue = colCell.String()
						}
					}

					//fmt.Printf("eng:%v  gsep:%v \n", stringValueX, stringValue)
					// have enough to update
					if stringValueX != "" && stringValue != "" {
						// compare db with update
						strID = common.StrValue2strID(db, stringValueX)
						// get dbvalue for gsep
						dbValue = common.StrID2strValue(db, strID, "rux")

						sectionID = common.StrID2sectionID(db, strID, "rux")
						stringID = strconv.Itoa(strID)
						// print/compare dbValue, gsep strings
						if stringID != "0" {
							// save the below 2 line for reporting
							//fmt.Printf("eng:%v id:%v secID:%v \n", stringValueX, strID, sectionID)
							//fmt.Printf("\t\t db:%v  gsep:%v \n\n", dbValue, stringValue)

							common.Dbstrings(db, stringID, stringValue, "rux", "4", currentTime, currentTime, currentTime, sectionID)
						} else {
							//fmt.Printf("eng:%v id:%v secID:%v \n", stringValueX, strID, sectionID)
							//fmt.Printf("\t not updated gsep:%v \n\n", stringValue)
						}
					}
				}

				// DB table for source and appInstance, move data, below changes alot!
				if dbsrcApp == true { //for 3 tables(strings,source,appInstance)
					for colnum, colCell := range row.Cells {
						switch colnum {
						case 0:
							stringID = colCell.String()
						case 1:
							stringKey = colCell.String()
							// have enough to update
							if stringID != "" {
								common.Dbsource(db, stringID, stringKey, "", "")
								common.DbappIn(db, stringID, historyID, "", "", "")
							}
						}
					}
				}

				//os.Exit(0) // use to stop after 1 run
			}
			//end of row number check
		}
		//end of rows check
	}
	//end of sheets check
}
