package main

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"

	//"strconv"
	"../common"
	"github.com/jmoiron/sqlx"
)

/// main() activeStrings.go, a program that exports an excel file from the mysql database to be used for a new gsep language
/// uses the runtime.ini file to control exported values (see runtime.ini)
func main() {

	npID, snID, stID, nState, nfillInTarget := common.Getini()
	switch npID {
	case "101": // capstone
		// ok
	case "102": // sparkvue
		// ok
	default:
		os.Exit(1) //nothing found
	}

	db, ferr := sqlx.Open("mysql", "root:3tring3DB@@@tcp(127.0.0.1:3306)/sourceLocalize?charset=utf8")
	common.ExitErr(ferr)
	defer db.Close()

	// check operation language code to see if if exsists
	country := common.PascoID2Country(db, snID)
	fmt.Printf("processing: sIDtarget=%s, neighbor country=%s, targetID=%s, targetState=%s fillInTarget=%s\n", npID, country, stID, nState, nfillInTarget)
	//os.Exit(1)

	var filename2, e string
	var tmpNum int
	filename2 = fmt.Sprintf("../../dataout/Reports/activeStringsID.tsv")
	fhandle2, ferr2 := os.Create(filename2)
	common.ExitErr(ferr2)
	defer fhandle2.Close()

	// get stringid must be in sync in both the source and strings table
	// currently the DB does not enforce this
	stmt := fmt.Sprintf("SELECT sectionID, stringID, state, stringValue FROM strings WHERE pascoID='enu' and (sectionID='%v' or sectionID=99) and state!=99 ORDER BY sectionID asc, stringID asc", npID)
	rows, err := db.Query(stmt)
	common.ExitErr(err)
	defer rows.Close()

	var sectionID, stringID, state, stringValue sql.NullString
	fmt.Fprintf(fhandle2, "sectionID\t stringID\tstringKey\tpascoID(target)\tstate(target)\tdevNotes\tdescription\tstringValue(for your local translation)\tEnglish(stringValue)\tneighbor(stringValue)\tpath(to feature)\tlocalizerNotes\targuments\n")

	for rows.Next() {
		err = rows.Scan(&sectionID, &stringID, &state, &stringValue)
		common.ExitErr(err)

		key := common.StrID2strKey(db, stringID.String)       // need to lookup stringKey for stringID
		d := common.StrID2devNotes(db, stringID.String)       //devnotes
		desc := common.StrID2description(db, stringID.String) //description
		if nfillInTarget == "1" {
			tmpNum, _ = strconv.Atoi(stringID.String)
			e = common.StrID2strValue(db, tmpNum, stID) // target language from strings
		} else {
			e = " " //empty spot to be filled in by gsep
		}
		tmpNum, _ = strconv.Atoi(stringID.String)
		nv := common.StrID2strValue(db, tmpNum, snID) // neighbor language to help translate
		//fmt.Printf("nv=%s, country=%s, tmp=%d str=%s\n", nv, nID, tmpNum, stringID.String)
		//os.Exit(1)
		p := common.StrID2path(db, stringID.String)            // path where it is in the application
		ln := common.StrID2localizerNotes(db, stringID.String) // localizer notes hints for gsep
		a := common.StrID2arguments(db, stringID.String)       // description of arguments

		fmt.Fprintf(fhandle2, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", sectionID.String, stringID.String, key, snID, nState, d, desc, e, stringValue.String, nv, p, ln, a)

	}
	err = rows.Err()
	common.ExitErr(err)
}
