package main

import (
	"database/sql"
	"flag"
	"fmt"
	"os"

	"../common"
	"github.com/jmoiron/sqlx"
	"github.com/tealeg/xlsx"
)

func usage() {
	fmt.Fprintf(os.Stderr, "\n\nusage: add any build flag\n\n")
	flag.PrintDefaults()
	os.Exit(2)
}

var testrun = false

/// main gsepxlsx.go, a prototype that produces seperate xls for all the languages from mysql
func main() {
	flag.Parse()
	if len(os.Args) < 1 {
		flag.PrintDefaults()
		return
	}

	db, ferr := sqlx.Open("mysql", "root:3tring3DB@@@tcp(127.0.0.1:3306)/sourceLocalize?charset=utf8")
	common.ExitErr(ferr)
	defer db.Close()

	stmt := "SELECT pascoID FROM code"
	langs, lerr := db.Query(stmt)
	common.ExitErr(lerr)
	defer langs.Close()

	var filename string
	var pascoID sql.NullString
	for langs.Next() {
		lerr = langs.Scan(&pascoID)
		common.ExitErr(lerr)

		filename = fmt.Sprintf("./reports/Spark_%v.xlsx", pascoID.String)
		// TODO NEED TO ADD PATH , NOTES ECT
		//checkCount
		stmt = fmt.Sprintf("SELECT COUNT(*) as count FROM strings WHERE (sectionID=99 or sectionid=101) AND pascoID='%v' AND state!=99 ", pascoID.String)
		results, err := db.Query(stmt)
		common.ExitErr(err)
		defer results.Close()

		// index to see when we have 1st line for json
		var countRows int
		for results.Next() {
			err := results.Scan(&countRows)
			common.ExitErr(err)
		}
		err = results.Err()
		common.ExitErr(err)
		results.Close()
		//end of checkCount

		stmt = fmt.Sprintf("SELECT * FROM strings WHERE (sectionID=99 or sectionid=101) AND pascoID='%v' AND state!=99 ORDER BY pascoID, stringID ", pascoID.String)
		rows, err := db.Query(stmt)
		common.ExitErr(err)
		defer rows.Close()

		var count = 1
		var reportbreak = "begining"
		xlFile := xlsx.NewFile()
		sheet, _ := xlFile.AddSheet("Sheet1")
		row := sheet.AddRow()
		cell1 := row.AddCell()
		cell2 := row.AddCell()
		cell3 := row.AddCell()
		cell4 := row.AddCell()
		cell5 := row.AddCell()
		cell6 := row.AddCell()
		cell7 := row.AddCell()
		cell8 := row.AddCell()
		cell9 := row.AddCell()
		cell10 := row.AddCell()
		cell11 := row.AddCell()
		cell12 := row.AddCell()
		cell13 := row.AddCell()

		cell1.Value = "stringID"
		cell2.Value = "stringKey"
		cell3.Value = "stringValue"
		cell4.Value = "pascoID"
		cell5.Value = "state"
		cell6.Value = "creation"
		cell7.Value = "modification"
		cell8.Value = "certified"
		cell9.Value = "sectionID"
		cell10.Value = "arguments"
		cell11.Value = "description"
		cell12.Value = "path"
		cell13.Value = "localizerNotes"

		var stringID, stringValue, pascoID, translation, creation, modification, certified, sectionID sql.NullString

		for rows.Next() {
			err = rows.Scan(&stringID, &stringValue, &pascoID, &translation, &creation, &modification, &certified, &sectionID)
			common.ExitErr(err)

			// TODO QUERY DATA FROM SOURCE AND APPINSTANCE
			if reportbreak == "begining" {
				// create xlsx and sheet headers
				xlFile := xlsx.NewFile()
				sheet, _ := xlFile.AddSheet("Sheet1")
				row := sheet.AddRow()
				cell1 := row.AddCell()
				cell2 := row.AddCell()
				cell3 := row.AddCell()
				cell4 := row.AddCell()
				cell5 := row.AddCell()
				cell6 := row.AddCell()
				cell7 := row.AddCell()
				cell8 := row.AddCell()
				cell9 := row.AddCell()
				cell10 := row.AddCell()
				cell11 := row.AddCell()
				cell12 := row.AddCell()
				cell13 := row.AddCell()

				cell1.Value = "stringID"
				cell2.Value = "stringKey"
				cell3.Value = "stringValue"
				cell4.Value = "pascoID"
				cell5.Value = "state"
				cell6.Value = "creation"
				cell7.Value = "modification"
				cell8.Value = "certified"
				cell9.Value = "sectionID"
				cell10.Value = "arguments"
				cell11.Value = "description"
				cell12.Value = "path"
				cell13.Value = "localizerNotes"

			}

			row = sheet.AddRow()
			cell1 := row.AddCell()
			cell2 := row.AddCell()
			cell3 := row.AddCell()
			cell4 := row.AddCell()
			cell5 := row.AddCell()
			cell6 := row.AddCell()
			cell7 := row.AddCell()
			cell8 := row.AddCell()
			cell9 := row.AddCell()
			cell10 := row.AddCell()
			cell11 := row.AddCell()
			cell12 := row.AddCell()
			cell13 := row.AddCell()

			cell1.Value = stringID.String
			key := common.StrID2strKey(db, stringID.String) // need to lookup stringKey for stringID
			cell2.Value = key
			cell3.Value = stringValue.String
			cell4.Value = pascoID.String
			cell5.Value = translation.String
			cell6.Value = creation.String
			cell7.Value = modification.String
			cell8.Value = certified.String
			cell9.Value = sectionID.String
			//TODO ADD REAL VALUES
			cell10.Value = sectionID.String
			cell11.Value = sectionID.String
			cell12.Value = sectionID.String
			cell13.Value = sectionID.String

			reportbreak = pascoID.String
			count++
		}
		err = rows.Err()
		common.ExitErr(err)

		err = xlFile.Save(filename)
	}
	err := langs.Err()
	common.ExitErr(err)

}
