package main

import (
	"database/sql"
	"fmt"

	//"strings"

	"../common"
	"github.com/jmoiron/sqlx"
	//_ "github.com/mattn/go-sqlite3"
)

/// main cleanDB.go, a program that changes data in mysql that cause problem for development
func main() {

	//dbOpen := "../data/sourceLocalize.db3"
	//db, ferr := sqlx.Open("sqlite3", dbOpen)
	//mydsn := os.Getenv("SQLX_MYSQL_DSN")
	db, ferr := sqlx.Open("mysql", "root:3tring3DB@@@tcp(127.0.0.1:3306)/sourceLocalize?charset=utf8")

	common.ExitErr(ferr)
	defer db.Close()

	/* used * here instead of each var we need
	strange bug with sql if we set each var to return from database
	may be a race condition in datbase
	[mysql] 2018/01/02 13:12:44 packets.go:72: unexpected EOF
	[mysql] 2018/01/02 13:12:44 packets.go:431: busy buffer
	e.g. "SELECT stringID,stringValue,pascoCode,typeID FROM strings"
	*/
	//might be better to drive this from source table, since there are duplicate stringValues, which are valid with differnet types
	stmt := fmt.Sprintf("SELECT * FROM strings")
	//fmt.Printf("%s\n", stmt)

	rows, err := db.Queryx(stmt)
	common.ExitErr(err)
	var count = 0
	defer rows.Close()

	var stringID, stringValue, pascoID, state, creation, modificationDate, certified, sectionID sql.NullString

	for rows.Next() {
		err = rows.Scan(&stringID, &stringValue, &pascoID, &state, &creation, &modificationDate, &certified, &sectionID)
		common.ExitErr(err)

		key := common.StrID2strKey(db, stringID.String) // need to lookup stringKey for stringID
		//fmt.Printf("value is:%s \n", idKeyValue)
		//os.Exit(0)

		// trim and remove control chars
		if key != "" {
			// must check string key source upon input (see xlxs2db)

			newValue := common.Valueclean(stringValue.String)
			if newValue != stringValue.String {
				stmt = fmt.Sprintf("UPDATE strings SET stringValue=\"%v\" WHERE stringID='%v' and pascoID='%v' and sectionID='%v'; ", newValue, stringID.String, pascoID.String, sectionID.String)
				common.ExecSQL(db, stmt)
				count = +1
			}
		}
	}
	err = rows.Err()
	common.ExitErr(err)
	fmt.Println(count)
}
