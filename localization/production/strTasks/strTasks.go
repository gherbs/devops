package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func printCommand(cmd *exec.Cmd) {
	fmt.Printf("==> Executing: %s\n", strings.Join(cmd.Args, " "))
}

func printError(err error) {
	if err == nil {
		os.Stderr.WriteString(fmt.Sprintf("==> Error: %s\n", err.Error()))
	}
}

func printOutput(outs []byte) {
	if len(outs) > 0 {
		fmt.Printf("==> Output: %s\n", string(outs))
	}
}

func doit(cmd string) {
	runcmd := exec.Command(cmd)
	// Combine stdout and stderr
	//printCommand(runcmd)
	//output, err :=
	runcmd.CombinedOutput()
	//printError(err)
	//printOutput(output)
}

/// main() strTasks.go, a prototype to script the admin task for mysql database
func main() {
	var p int
	//program must run as root chmod +s
	fmt.Println("1 backup database")
	fmt.Println("2 restore database")
	fmt.Println("0 quit program")
loop:
	for _, err := fmt.Scanf("%d", &p); p != 0 || err != nil; _, err = fmt.Scanf("%d", &p) {
		switch p {
		case 0:
			fmt.Println("zero", p)
		case 1:
			doit("/usr/local/mysql/bin/mysqldump -u root --password=3tring3DB@@ sourceLocalize source strings section code history appInstances category subCategory > /tmp/productionDB.sql")
			break loop
		default:
			fmt.Println("got", p)
			doit("ls -la >test")
		}
	}
	fmt.Println("done")
	/*
		for choice != 9 {
			fmt.Scanf("%s", &choice)

			//doit("mysqldump", " -u root --password=3tring3DB@@ sourceLocalize source strings section code history appInstances category subCategory > productionDB.sql")
			//doit("myql","u root --password=3tring3DB@@ sourceLocalize < productionDB.sql")
		}
	*/
}
