package common

import (
	//"database/sql"
	"encoding/hex"
	"fmt"
	"os"
	"strings"

	"github.com/go-ini/ini"
	_ "github.com/go-sql-driver/mysql" //needed for database
	"github.com/jmoiron/sqlx"
)

//globals
var debug string // set this to 1 to just print sql and not execute it

/// Getini() load the ini file
func Getini() (string, string, string, string, string) {
	cfg, err := ini.Load("./runtime.ini")
	ExitErr(err)
	name := cfg.Section("values").Key("name").String()     // filename
	param1 := cfg.Section("values").Key("param1").String() // parameter 1
	param2 := cfg.Section("values").Key("param2").String() // parameter 2
	param3 := cfg.Section("values").Key("param3").String() // parameter 3
	param4 := cfg.Section("values").Key("param4").String() // parameter 3
	debug = cfg.Section("values").Key("debug").String()    //
	return name, param1, param2, param3, param4
}

/// ExecSql() execute sql and handle errors
func ExecSQL(db *sqlx.DB, stmt string) {
	if debug == "1" {
		Debug(stmt)
	} else {
		_, err := db.Exec(stmt)
		dbgsqlErr(stmt, err)
		//Debug(stmt)
	}
}

/// StrID2strKey() stringKey for stringID
func StrID2strKey(db *sqlx.DB, stringID string) string {
	stmt := fmt.Sprintf("SELECT stringKey FROM source WHERE stringID='%v'", stringID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var key string
	for results.Next() {
		err = results.Scan(&key)
		ExitErr(err)
	}
	return (key)
}

/// StrID2strValue() stringValue for stringID for a given pascoID
func StrID2strValue(db *sqlx.DB, stringID int, pascoID string) string {
	stmt := fmt.Sprintf("SELECT stringValue FROM strings WHERE stringID='%v' and pascoID='%v' ", stringID, pascoID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value string
	for results.Next() {
		err = results.Scan(&value)
		ExitErr(err)
	}
	return (value)
}

/// StrID2sectionID() sectionID for stringID for a given pascoID
func StrID2sectionID(db *sqlx.DB, stringID int, pascoID string) string {
	stmt := fmt.Sprintf("SELECT sectionID FROM strings WHERE stringID='%v' and pascoID='%v' ", stringID, pascoID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value string
	for results.Next() {
		err = results.Scan(&value)
		ExitErr(err)
	}
	return (value)
}

/// StrID2description() get description for stringID
func StrID2description(db *sqlx.DB, stringID string) string {
	stmt := fmt.Sprintf("SELECT description FROM source WHERE stringID='%v' ", stringID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value string
	for results.Next() {
		err = results.Scan(&value)
		SkipErr(err)
	}
	return (value)
}

/// StrID2arguments() get arguments for stringID
func StrID2arguments(db *sqlx.DB, stringID string) string {
	stmt := fmt.Sprintf("SELECT arguments FROM source WHERE stringID='%v' ", stringID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value string
	for results.Next() {
		err = results.Scan(&value)
		SkipErr(err)
	}
	return (value)
}

/// StrID2localizerNotes() get localizerNotes for stringID
func StrID2localizerNotes(db *sqlx.DB, stringID string) string {
	stmt := fmt.Sprintf("SELECT localizerNotes FROM appInstances WHERE stringID='%v' ", stringID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value string
	for results.Next() {
		err = results.Scan(&value)
		SkipErr(err)
	}
	return (value)
}

/// StrID2category() get category for stringID
func StrID2category(db *sqlx.DB, stringID string) string {
	stmt := fmt.Sprintf("SELECT categoryID FROM appInstances WHERE stringID='%v' ", stringID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value string
	for results.Next() {
		err = results.Scan(&value)
		SkipErr(err)
	}
	return (value)
}

/// StrID2subCategory() get subCategory for stringID
func StrID2subCategory(db *sqlx.DB, stringID string) string {
	stmt := fmt.Sprintf("SELECT subCategoryID FROM appInstances WHERE stringID='%v' ", stringID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value string
	for results.Next() {
		err = results.Scan(&value)
		SkipErr(err)
	}
	return (value)
}

/// StrID2path() get path for stringID
func StrID2path(db *sqlx.DB, stringID string) string {
	stmt := fmt.Sprintf("SELECT path FROM appInstances WHERE stringID='%v' ", stringID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value string
	for results.Next() {
		err = results.Scan(&value)
		SkipErr(err)
	}
	return (value)
}

/// StrID2devNotes() get devNotes for stringID
func StrID2devNotes(db *sqlx.DB, stringID string) string {
	stmt := fmt.Sprintf("SELECT devNotes FROM appInstances WHERE stringID='%v' ", stringID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value string
	for results.Next() {
		err = results.Scan(&value)
		SkipErr(err)
	}
	return (value)
}

/// StrValue2strID() get stringID for stringValue
func StrValue2strID(db *sqlx.DB, stringValue string) int {
	stmt := fmt.Sprintf("SELECT stringID FROM strings WHERE stringValue=\"%v\" and pascoID='enu' ", stringValue)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var value int
	for results.Next() {
		err = results.Scan(&value)
		ExitErr(err)
	}
	return (value)
}

/// StrKey2strID() get the stringID for a stringKey
func StrKey2strID(db *sqlx.DB, stringKey string) string {
	stmt := fmt.Sprintf("SELECT stringID FROM source USE INDEX(sourceindex) WHERE stringKey='%v' ", stringKey)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var id string
	for results.Next() {
		err = results.Scan(&id)
		ExitErr(err)
	}
	return (id)
}

/// StrKey2sectionID() Get sectionID for a stringKey and enu
func StrKey2sectionID(db *sqlx.DB, stringKey string, sectionID string) string {
	//look up stringid
	stringid := StrKey2strID(db, stringKey) // need to lookup stringID for stringKey
	//then get sectionid for enu and stringID
	stmt := fmt.Sprintf("SELECT sectionID FROM strings WHERE stringID='%v' AND pascoID='enu' ", stringid)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var stringID string
	for results.Next() {
		err = results.Scan(&stringID)
		ExitErr(err)
	}
	return stringID
}

/// SectionID2name() get the sectionID from name
func SectionID2name(db *sqlx.DB, sectionID string) string {
	stmt := fmt.Sprintf("SELECT name FROM section WHERE sectionID='%v'", sectionID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var name string
	for results.Next() {
		err = results.Scan(&name)
		ExitErr(err)
	}
	return (name)
}

/// ColNum2PascoID*() get the ordering from code
func ColNum2PascoID(db *sqlx.DB, colNum string) string {
	stmt := fmt.Sprintf("SELECT pascoID FROM code WHERE colNum='%v'", colNum)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var id string
	for results.Next() {
		err = results.Scan(&id)
		ExitErr(err)
	}
	return (id)
}

/// PascoID2Country get the country from pascoID
func PascoID2Country(db *sqlx.DB, pascoID string) string {
	stmt := fmt.Sprintf("SELECT country FROM code WHERE pascoID='%v'", pascoID)
	//Debug(stmt)
	results, err := db.Query(stmt)
	ExitErr(err)
	var id string
	for results.Next() {
		err = results.Scan(&id)
		ExitErr(err)
	}
	return (id)
}

/// ExitErr template for errors
func ExitErr(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

/// SkipErr not required in DB
func SkipErr(err error) {
	if err != nil {
		return
	}
}

/// dbgsqlErr handler
func dbgsqlErr(stmt string, err error) {
	if err != nil {
		fmt.Println(err, stmt)
		os.Exit(1)
		//return
	}
}

/// Debug output
func Debug(err string) {
	fmt.Printf("Debug: %s\n", err)
}

/// DbappInstances insert into appInstances
func DbappInstances(db *sqlx.DB, stringID, historyID, path, localizerNotes, devNotes, categoryID, subCategoryID string) {
	var countRows int
	// check for key if found update, else insert
	stmt := fmt.Sprintf("SELECT count(1) AS count FROM appInstances WHERE stringID='%v' AND historyID='%v'", stringID, historyID)
	countRows = Dbcountrow(db, stmt)

	if countRows > 0 {
		stmt := fmt.Sprintf("UPDATE appInstances SET historyID='%v', path=\"%v\",localizerNotes=\"%v\",devNotes=\"%v\",categoryID='%v',subCategoryID='%v' WHERE stringID='%v'  ", historyID, path, localizerNotes, devNotes, categoryID, subCategoryID, stringID)
		ExecSQL(db, stmt)
	} else {
		stmt := fmt.Sprintf("INSERT INTO appInstances (stringID,historyID,path,localizerNotes,devNotes,categoryID,subCategoryID) VALUES('%v','%v',\"%v\",\"%v\",\"%v\",\"%v\",'%v')", stringID, historyID, path, localizerNotes, devNotes, categoryID, subCategoryID)
		ExecSQL(db, stmt)
	}
}

/// DbappIn() insert into appInstances (short version)
func DbappIn(db *sqlx.DB, stringID string, historyID int, path string, localizerNotes string, devNotes string) {
	var countRows int
	// check for key if found update, else insert
	stmt := fmt.Sprintf("SELECT count(1) AS count FROM appInstances WHERE stringID='%v' AND historyID='%v'", stringID, historyID)
	countRows = Dbcountrow(db, stmt)

	if countRows > 0 {
		stmt := fmt.Sprintf("UPDATE appInstances SET path=\"%v\",localizerNotes=\"%v\",devNotes=\"%v\" WHERE stringID='%v' AND historyID='%v'", path, localizerNotes, devNotes, stringID, historyID)
		ExecSQL(db, stmt)
	} else {
		stmt := fmt.Sprintf("INSERT INTO appInstances (stringID,historyID,path,localizerNotes,devNotes) VALUES('%v','%v',\"%v\",\"%v\",\"%v\")", stringID, historyID, path, localizerNotes, devNotes)
		ExecSQL(db, stmt)
	}
}

/// Dbsource() insert into source
func Dbsource(db *sqlx.DB, stringID, stringKey, arguments, description string) {
	var countRows int
	// arguments need esc for '
	// description need esc for '
	// check for key if found update, else insert
	stmt := fmt.Sprintf("SELECT count(1) AS count FROM source WHERE stringID='%v' AND stringKey='%v' ", stringID, stringKey)
	countRows = Dbcountrow(db, stmt)

	if countRows > 0 {
		stmt := fmt.Sprintf("UPDATE source SET arguments=\"%v\",description=\"%v\" WHERE stringID='%v' AND stringKey='%v' ", arguments, description, stringID, stringKey)
		ExecSQL(db, stmt)
	} else {
		stmt := fmt.Sprintf("INSERT INTO source (stringID,stringKey,arguments,description) VALUES('%v','%v',\"%v\",\"%v\") ", stringID, stringKey, arguments, description)
		ExecSQL(db, stmt)
	}
}

/// Dbstrings() insert into strings
func Dbstrings(db *sqlx.DB, stringID, stringValue, pascoID, state, creation, modification, certified, sectionID string) {
	var countRows int
	stmt := fmt.Sprintf("SELECT count(1) AS count FROM strings WHERE stringID='%v' AND pascoID='%v' AND sectionID='%v' ", stringID, pascoID, sectionID)
	countRows = Dbcountrow(db, stmt)

	// clean stringValue
	newValue := strings.TrimSpace(StripCtlFromUTF8(stringValue))
	newValue = Valueclean(newValue)

	if countRows > 0 {
		stmt := fmt.Sprintf("UPDATE strings SET stringValue=\"%v\",state='%v',creation='%v',modification='%v',certified='%v' WHERE stringID='%v' AND pascoID='%v' AND sectionID='%v' ", newValue, state, creation, modification, certified, stringID, pascoID, sectionID)
		ExecSQL(db, stmt)
	} else {
		// stringValue need esc for '
		// check for key if found update, else insert
		stmt := fmt.Sprintf("INSERT INTO strings (stringID,stringValue,pascoID,state,creation,modification,certified,sectionID) VALUES('%v',\"%v\",'%v','%v','%v','%v','%v','%v')", stringID, newValue, pascoID, state, creation, modification, certified, sectionID)
		ExecSQL(db, stmt)
	}
}

/// Dbstr() insert into strings (the short version)
func Dbstr(db *sqlx.DB, stringID, pascoID string) {
	var countRows int
	stmt := fmt.Sprintf("SELECT count(1) AS count FROM strings WHERE stringID='%v' AND pascoID='%v' ", stringID, pascoID)
	countRows = Dbcountrow(db, stmt)

	if countRows > 0 {
		stmt := fmt.Sprintf("UPDATE strings SET pascoID='%v' WHERE stringID='%v' ", stringID, pascoID)
		ExecSQL(db, stmt)
	} else {
		// stringValue need esc for '
		// check for key if found update, else insert
		stmt := fmt.Sprintf("INSERT INTO strings (stringID,pascoID) VALUES('%v','%v')", stringID, pascoID)
		ExecSQL(db, stmt)
	}
}

/// Countstring() to see if value is correct in the strings table
func Countstring(db *sqlx.DB, stringID string, pascoID string, sectionID string) int {
	var countRows int
	stmt := fmt.Sprintf("SELECT count(1) AS count FROM strings WHERE stringID='%v' AND pascoID='%v' AND sectionID='%v' ", stringID, pascoID, sectionID)
	countRows = Dbcountrow(db, stmt)
	return countRows
}

/// Dbcountrow() return number of rows in query
func Dbcountrow(db *sqlx.DB, stmt string) int {
	results, err := db.Query(stmt)
	ExitErr(err)
	defer results.Close()

	var countRows int
	for results.Next() {
		err := results.Scan(&countRows)
		ExitErr(err)
	}
	err = results.Err()
	ExitErr(err)

	return (countRows)
}

/// StripCtlFromUTF8() remove control code
func StripCtlFromUTF8(str string) string {
	return strings.Map(func(r rune) rune {
		if r >= 32 && r != 127 {
			return r
		}
		return -1
	}, str)
}

/// Valueclean() remove bad json char in stringValue
func Valueclean(newStr string) string {

	const s = "efbc85"
	decoded, serr := hex.DecodeString(s)
	ExitErr(serr)
	widepercent := string(decoded)

	const a = "d9aa"
	decoded, aerr := hex.DecodeString(a)
	ExitErr(aerr)
	arxpercent := string(decoded)

	const d = "c2a0"
	decoded, derr := hex.DecodeString(d)
	ExitErr(derr)
	nbsp := string(decoded)

	//change wide %
	if strings.Contains(newStr, widepercent) {
		newStr = strings.Replace(newStr, widepercent, "%", -1)
	}

	//change arx %
	if strings.Contains(newStr, arxpercent) {
		newStr = strings.Replace(newStr, arxpercent, "%", -1)
	}

	//remove \
	if strings.Contains(newStr, "\\") {
		newStr = strings.Replace(newStr, "\\", "", -1)
	}

	//change non breaking space to space
	if strings.Contains(newStr, nbsp) {
		newStr = strings.Replace(newStr, nbsp, " ", -1)
	}

	//change % 1
	if strings.Contains(newStr, "% 1") {
		newStr = strings.Replace(newStr, "% 1", "%1", -1)
	}

	//change % 2
	if strings.Contains(newStr, "% 2") {
		newStr = strings.Replace(newStr, "% 2", "%2", -1)
	}

	//change % 3
	if strings.Contains(newStr, "% 3") {
		newStr = strings.Replace(newStr, "% 3", "%3", -1)
	}

	//change % 4
	if strings.Contains(newStr, "% 4") {
		newStr = strings.Replace(newStr, "% 4", "%4", -1)
	}

	//change double quote to single quote
	if strings.Contains(newStr, "\"") {
		newStr = strings.Replace(newStr, "\"", "'", -1)
	}

	return newStr
}

/// Keyclean() fix stringKey
func Keyclean(newKey, typeID string) string {
	// remove -
	if typeID != "5" {
		if strings.Contains(newKey, "-") {
			newKey = strings.Replace(newKey, "-", "", -1)
		}
	}

	// remove *
	if strings.Contains(newKey, "*") {
		newKey = strings.Replace(newKey, "*", "", -1)
	}

	return newKey
}
