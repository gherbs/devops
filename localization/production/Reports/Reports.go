package main

import (
	"database/sql"
	"fmt"
	"os"

	"strconv"

	"../common"
	"github.com/jmoiron/sqlx"
)

/// main Reports.go, a program to create xls for development review of the mysql data
func main() {

	db, ferr := sqlx.Open("mysql", "root:3tring3DB@@@tcp(127.0.0.1:3306)/sourceLocalize?charset=utf8")
	common.ExitErr(ferr)
	defer db.Close()

	var filename2 string

	filename2 = fmt.Sprintf("../../dataout/Reports/DataSheetStringsID.tsv")
	fhandle2, ferr2 := os.Create(filename2)
	common.ExitErr(ferr2)
	defer fhandle2.Close()

	// get stringid must be in sync in both the source and strings table
	// currently the DB does not enforce this
	stmt := fmt.Sprintf("SELECT stringID, stringValue, sectionID FROM strings WHERE pascoID='enu' and state !=99 ORDER BY sectionID asc, stringID asc")
	rows, err := db.Query(stmt)
	common.ExitErr(err)
	defer rows.Close()

	var desc, a, l, c, s, p, o, d string
	var x int
	var stringID, stringValue, sectionID sql.NullString
	fmt.Fprintf(fhandle2, "sectionID\tstringID\tstringKey\tdescription\targuments\tlocalizerNotes\tcategoryID\tsubCategoryID\tpath\tstringValue\toldCap#\tdevNotes\n")

	for rows.Next() {
		err = rows.Scan(&stringID, &stringValue, &sectionID)
		common.ExitErr(err)

		key := common.StrID2strKey(db, stringID.String) // need to lookup stringKey for stringID
		desc = common.StrID2description(db, stringID.String)
		a = common.StrID2arguments(db, stringID.String)

		switch sectionID.String {
		case "0", "1", "2", "3", "4", "5", "6", "7":
			{
				l = ""
				c = ""
				s = ""
				p = ""
				o = ""
				d = ""
			}

		case "99", "102", "103", "104":
			{
				l = common.StrID2localizerNotes(db, stringID.String)
				c = common.StrID2category(db, stringID.String)
				s = common.StrID2subCategory(db, stringID.String)
				p = common.StrID2path(db, stringID.String)
				d = common.StrID2devNotes(db, stringID.String)
				o = ""
			}
		case "101":
			{
				l = common.StrID2localizerNotes(db, stringID.String)
				c = common.StrID2category(db, stringID.String)
				s = common.StrID2subCategory(db, stringID.String)
				p = common.StrID2path(db, stringID.String)

				x, _ = strconv.Atoi(stringID.String)

				if x >= 100000 {
					o = strconv.Itoa(x - 100000)
				} else {
					o = ""
				}

				d = common.StrID2devNotes(db, stringID.String)
			}
		}

		fmt.Fprintf(fhandle2, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", sectionID.String, stringID.String, key, desc, a, l, c, s, p, stringValue.String, o, d)

	}
	err = rows.Err()
	common.ExitErr(err)
}
