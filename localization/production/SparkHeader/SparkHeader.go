package main

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"
	"time"

	"../common"
	"github.com/jmoiron/sqlx"
)

var testrun = false

/// main() SparkHeader.go, a program to create header files for sparkvue development from the mysql database
func main() {
	var pheader1 = `//-------------------------------------------------------------------------------------------------
/// PASCO Scientific
/// Copyright (c) 2018 PASCO Scientific. All rights reserved.
///
/// file   Common/SPARKvueUI/workspace/SPARKvueUI/public/javascripts `
	var pheader2 = `/// brief  Declares the string resource identifiers
/// note   Automatically Generated by [Sparkheader.go]
///                      DO NOT EDIT MANUALLY!!!
//------------------------------------------------------------------------------------------------- `

	db, ferr := sqlx.Open("mysql", "root:3tring3DB@@@tcp(127.0.0.1:3306)/sourceLocalize?charset=utf8")

	common.ExitErr(ferr)
	defer db.Close()

	filename := fmt.Sprintf("../../dataout//SparkHeaderString/StringResource.d.ts")
	fhTS, ferr := os.Create(filename)
	common.ExitErr(ferr)
	defer fhTS.Close()

	file2name := fmt.Sprintf("../../dataout/SparkHeaderString/StringResource.js")
	fhJS, ferr := os.Create(file2name)
	common.ExitErr(ferr)
	defer fhJS.Close()

	file3name := fmt.Sprintf("../../dataout/SparkHeaderString/StringResource.h")
	fhH, ferr := os.Create(file3name)
	common.ExitErr(ferr)
	defer fhH.Close()

	t := time.Now()
	currentTime := t.Format("20060102150405") //use const to get yyyyMMddHHmmss

	fmt.Fprintf(fhJS, "\n%s\n", pheader1)
	fmt.Fprintf(fhJS, "/// date  [%s]", currentTime)
	fmt.Fprintf(fhJS, "\n%s\n", pheader2)

	fmt.Fprintf(fhTS, "\n%s\n", pheader1)
	fmt.Fprintf(fhTS, "/// date  [%s]", currentTime)
	fmt.Fprintf(fhTS, "\n%s\n", pheader2)

	fmt.Fprintf(fhH, "\n%s\n", pheader1)
	fmt.Fprintf(fhH, "/// date  [%s]", currentTime)
	fmt.Fprintf(fhH, "\n%s\n", pheader2)

	stmt := fmt.Sprintf("SELECT source.stringID, stringKey, strings.stringID, state FROM source INNER JOIN strings ON source.stringID=strings.stringID WHERE (strings.sectionID='102' OR strings.sectionID=99) AND state!=99 AND pascoID='enu' ")

	rows, err := db.Query(stmt)
	common.ExitErr(err)
	defer rows.Close()

	var stringID, stringKey, appStringID, appActive sql.NullString
	var stringValue string
	var tmpNum int
	for rows.Next() {
		err = rows.Scan(&stringID, &stringKey, &appStringID, &appActive)
		common.ExitErr(err)
		//core
		fmt.Fprintf(fhJS, "var %v = %v;\n", stringKey.String, stringID.String)
		tmpNum, _ = strconv.Atoi(stringID.String)
		stringValue = common.StrID2strValue(db, tmpNum, "enu")
		fmt.Fprintf(fhTS, "declare let %v:number;\t//%v=%v\n", stringKey.String, stringID.String, stringValue)
		//is app =1
		fmt.Fprintf(fhH, "#define %v\t%v\n", stringKey.String, stringID.String)
		//end core

	}
	err = rows.Err()
	common.ExitErr(err)
}
