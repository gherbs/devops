rem !/bin/bash
rem set -x
rem " must be in folder with  productionDB.sql"
rem " e.g. ~/Documents/work_src/localization/production/savedDB/"
rem "Please use caution"
cd e:/ghs/work_src/localization/production/savedDB

mysql -u root --password=3tring3DB@@ sourceLocalize < productionDB.sql
rem  ps -ax | grep mysql
rem  mysql -u root -p

exit

echo "Do you wish to delete and reinstall the database with production.sql"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) make install; break;;
        No ) exit;;
    esac
done