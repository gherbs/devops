rem build in local dir e.g
rem must have mount point on NAS to share
cd e:/ghs/work_src/localization/production

cd CapHeader/
go build CapHeader.go
mv CapHeader.exe ../bin

cd ../Capstone/
go build Capstone.go
mv Capstone.exe ../bin

cd ../CleanDB/
go build CleanDB.go
mv CleanDB.exe ../bin

cd ../Datasheet/
go build Datasheet.go
mv Datasheet.exe ../bin

cd ../Reports/
go build Reports.go
mv Reports.exe ../bin

cd ../SparkHeader/
go build SparkHeader.go
mv SparkHeader.exe ../bin

cd ../SparkJson/
go build SparkJson.go
mv SparkJson.exe ../bin

cd ../strTasks/
go build strTasks.go
mv strTasks.exe ../bin

cd ../xlsx2db/
go build xlsx2db.go
mv xlsx2db.exe ../bin

cd ../PascoSystemUtility/
go build PascoSystemUtility.go
mv PascoSystemUtility.exe ../bin

#
rem tools
cd ../jsonDiff/
go build jsondiff.go
mv jsonDiff.exe ../../dataout/tools/

cd ../jsonUnpack/
go build jsonUnpack.go
mv jsonUnpack.exe ../../dataout/tools/
