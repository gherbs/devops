# Set-PSDebug -Trace 1
# build in local dir e.g
# must have mount point on NAS to share
cd e:/ghs/work_src/localization/production

cd CapHeader/
$env:GOOS = "windows"; $env:GOARCH = "amd64";go build CapHeader.go
mv -force CapHeader.exe ../bin

cd ../Capstone/
go build Capstone.go
mv -force Capstone.exe ../bin

cd ../CleanDB/
go build CleanDB.go
mv -force CleanDB.exe ../bin

cd ../Datasheet/
go build Datasheet.go
mv -force Datasheet.exe ../bin

cd ../Reports/
go build Reports.go
mv -force Reports.exe ../bin

cd ../SparkHeader/
go build SparkHeader.go
mv -force SparkHeader.exe ../bin

cd ../SparkJson/
go build SparkJson.go
mv -force SparkJson.exe ../bin

cd ../strTasks/
go build strTasks.go
mv -force strTasks.exe ../bin

cd ../xlsx2db/
go build xlsx2db.go
mv -force xlsx2db.exe ../bin

cd ../PascoSystemUtility/
go build PascoSystemUtility.go
mv -force PascoSystemUtility.exe ../bin

#
# tools
cd ../jsonDiff/
go build jsondiff.go
mv -force jsonDiff.exe ../../dataout/tools/
#linux version
#$env:GOOS = "linux"; $env:GOARCH = "amd64"; go build -o jsondiff jsondiff.go
#mv -force jsonDiff. ../../dataout/tools/


cd ../jsonUnpack/
go build jsonUnpack.go
mv -force jsonUnpack.exe ../../dataout/tools/
#linux version
#$env:GOOS = "linux"; $env:GOARCH = "amd64"; go build -o jsonUnpack jsonUnpack.go
#mv -force jsonUnpack ../../dataout/tools/

# go build Launcher.go