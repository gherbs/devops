rem awesome-go.com
rem https://github.com/avelino/awesome-go
rem https://go-search.org/
rem tools used in the strings project
go get -u -d -f -v github.com/go-sql-driver/mysql
go get -u -d -f -v github.com/jmoiron/sqlx
go get -u -d -f -v gopkg.in/gomail.v2
go get -u -d -f -v github.com/tealeg/xlsx
go get -u -d -f -v github.com/urfave/cli
go get -u -d -f -v github.com/yudai/gojsondiff

rem  tools that support the IDE
go get -u -v github.com/ramya-rao-a/go-outline
go get -u -v github.com/acroca/go-symbols
go get -u -v github.com/mdempsky/gocode
go get -u -v github.com/rogpeppe/godef
go get -u -v golang.org/x/tools/cmd/godoc
go get -u -v github.com/zmb3/gogetdoc
go get -u -v github.com/golang/lint/golint
go get -u -v github.com/fatih/gomodifytags
go get -u -v golang.org/x/tools/cmd/gorename
go get -u -v sourcegraph.com/sqs/goreturns
go get -u -v golang.org/x/tools/cmd/goimports
go get -u -v github.com/cweill/gotests/...
go get -u -v golang.org/x/tools/cmd/guru
go get -u -v github.com/josharian/impl
go get -u -v github.com/haya14busa/goplay/cmd/goplay
go get -u -v github.com/uudashr/gopkgs/cmd/gopkgs
go get -u -v github.com/davidrjenni/reftools/cmd/fillstruct
go get -u -v github.com/alecthomas/gometalinter
gometalinter --install
rem misc tools see https://golanglibs.com
go get -u -v github.com/humanfromearth/fnd
go get -u -v github.com/svent/sift

go get -u -v github.com/jesseduffield/lazygit
rem go get -u -v github.com/chasinglogic/fnd
go get -u -v github.com/Skarlso/goquestwebapp
go get -u -v github.com/derekparker/delve/cmd/dlv
go get -u -v github.com/astaxie/beego
go get -u -v github.com/beego/bee
rem  below fails under beta
go get -u -v github.com/wallix/awless
go get -u -v github.com/securego/gosec