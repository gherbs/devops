rem !/bin/bash
rem set -x
rem  build in local dir e.g
rem  /Users/georgeshields/
rem  must have mount point on NAS to share
rem export RunPoint=~/Documents/work_src/localization/production/bin

cd e:/ghs/work_src/localization/dataout
rm all.zip
rm -rf SparkJsonString
rm -rf SparkHeaderString
rm -rf CapHeaderString
rm -rf CapString
rm -rf DataSheetString
rm -rf Reports

mkdir SparkJsonString
mkdir SparkHeaderString
mkdir CapHeaderString
mkdir CapString
mkdir DataSheetString
mkdir Reports

cd e:/ghs/work_src/localization/production/bin
cleanDB
SparkJson
SparkHeader
CapHeader
Capstone
Datasheet
Reports
PascoSystemUtility

cp PascoSystemUtility/values-en/* PascoSystemUtility/values

cp PascoSystemUtility/values-zh-rCN/* PascoSystemUtility/values-za-rCN
cp PascoSystemUtility/values-zh-rCN/* PascoSystemUtility/values-zh-rSG
cp PascoSystemUtility/values-zh-rTW/* PascoSystemUtility/values-zh-rHK
cp PascoSystemUtility/values-zh-rTW/* PascoSystemUtility/values-zh-rMO
cp PascoSystemUtility/values-zh-rCN/* PascoSystemUtility/values-ii-rCN
cp PascoSystemUtility/values-zh-rCN/* PascoSystemUtility/values-ug-rCN

rem ./Launcher
rem cp Launcher/values-en/* Launcher/values/*
cd e:/ghs/work_src/localization/dataout
rem zip -r all.zip *
7z a -r all.zip *