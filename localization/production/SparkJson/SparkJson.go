package main

import (
	"database/sql"

	"fmt"
	"os"

	"../common"
	"github.com/jmoiron/sqlx"
)

var testrun = false

/// main() SparkJason.go, a program to create the Json file need for sparkvue development from the mysql database
func main() {
	//var translateTmp = false

	db, ferr := sqlx.Open("mysql", "root:3tring3DB@@@tcp(127.0.0.1:3306)/sourceLocalize?charset=utf8")
	common.ExitErr(ferr)
	defer db.Close()

	// for each language
	stmt := "SELECT pascoID FROM code"
	langs, lerr := db.Query(stmt)
	common.ExitErr(lerr)
	defer langs.Close()

	xfilename := fmt.Sprintf("../../dataout/SparkJsonString/Spark_bin.json")
	xfh, ferr := os.Create(xfilename)
	common.ExitErr(ferr)
	defer xfh.Close()
	fmt.Fprintf(xfh, "{")
	//var prevlang = ""
	var filename, debugname string
	var pascoID sql.NullString

	for langs.Next() {
		lerr = langs.Scan(&pascoID)
		common.ExitErr(lerr)

		//TODO CREAT A NEW FILE HANDLE AND OURPUT UNMIN DATA FOR COMPARE
		//fmt.Printf("processing lang : %v\n", pascoID.String)
		debugname = fmt.Sprintf("../../dataout/SparkJsonStringUnminify/Spark_%v.json", pascoID.String)
		fhandle, ferr := os.Create(debugname)
		common.ExitErr(ferr)
		defer fhandle.Close()

		filename = fmt.Sprintf("../../dataout/SparkJsonString/Spark_%v.json", pascoID.String)
		fhandle2, ferr2 := os.Create(filename)
		common.ExitErr(ferr2)
		defer fhandle2.Close()

		stmt = fmt.Sprintf("SELECT COUNT(1) as count FROM strings where (sectionID=99 or sectionID=102) AND pascoID='%v' AND state!=99 ", pascoID.String)
		results, err := db.Query(stmt)
		common.ExitErr(err)
		defer results.Close()

		// index to see when we have 1st line for json
		var countRows int
		for results.Next() {
			err := results.Scan(&countRows)
			common.ExitErr(err)
		}
		err = results.Err()
		common.ExitErr(err)
		results.Close()
		//end of checkCount

		stmt = fmt.Sprintf("SELECT stringID, stringValue, pascoID FROM strings WHERE (sectionID=99 or sectionID=102) AND pascoID='%v' AND state!=99 ORDER BY stringID", pascoID.String)

		rows, err := db.Query(stmt)
		common.ExitErr(err)
		defer rows.Close()

		var count = 1
		//var reportBreak = "begining"
		var stringID, stringValue, pascoID sql.NullString

		for rows.Next() {
			err = rows.Scan(&stringID, &stringValue, &pascoID)
			common.ExitErr(err)

			// core output
			if count == 1 {
				// header
				fmt.Fprintf(fhandle, "{\n")
				fmt.Fprintf(fhandle2, "{")
			}
			/*
				if translateTmp {
					if translation.String == "0" {
						stringValue.String = "!" + stringValue.String + "!"
					}
				}
			*/
			if count < countRows {
				if pascoID.String == "enu" {
					fmt.Fprintf(xfh, "\"%s\":\"%s\",\n", stringID.String, stringID.String)
					fmt.Fprintf(fhandle, "\"%s\":\"%s\",\n", stringID.String, stringValue.String)
					fmt.Fprintf(fhandle2, "\"%s\":\"%s\",", stringID.String, stringValue.String)
				} else {
					fmt.Fprintf(fhandle, "\"%s\":\"%s\",\n", stringID.String, stringValue.String)
					fmt.Fprintf(fhandle2, "\"%s\":\"%s\",", stringID.String, stringValue.String)
				}
			} else {
				if pascoID.String == "enu" {
					fmt.Fprintf(xfh, "\"%s\":\"%s\"", stringID.String, stringID.String)
					fmt.Fprintf(fhandle, "\"%s\":\"%s\"", stringID.String, stringValue.String)
					fmt.Fprintf(fhandle2, "\"%s\":\"%s\"", stringID.String, stringValue.String)
				} else {
					fmt.Fprintf(fhandle, "\"%s\":\"%s\"", stringID.String, stringValue.String)
					fmt.Fprintf(fhandle2, "\"%s\":\"%s\"", stringID.String, stringValue.String)
				}
			}
			// end core
			//reportBreak = pascoID.String
			count++
		}
		err = rows.Err()
		common.ExitErr(err)

		// extra fiddly bit to close json
		fmt.Fprintf(fhandle, "\n}")
		fmt.Fprintf(fhandle2, "}")
	}
	err := langs.Err()
	common.ExitErr(err)
	// extra fiddly bit to close json on last file
	fmt.Fprintf(xfh, "\n}")
}
